% building a static website
% /dev/urandom
% april 2020

If you take a look at the
[Makefile](https://gitlab.com/dev_urandom/simple-site/-/blob/master/Makefile) in
the source code repository of this website, you'll find that it's relatively
simple. Almost all of the heavy lifting is performed by two tools:

* "theme" from the ["discount"](http://www.pell.portland.or.us/~orc/
Code/discount/) Markdown parser

* "cpp", the C preprocessor

"theme" transforms pages, written in Markdown with an occasional piece of raw
HTML, into full-fledged webpages with the use of a simple template system. The
only additional rule is "style", inserted so that individual pages could have
their own CSS styles if they need it.

The C preprocessor, so far, is only used on the CSS style. It uses a bunch of
good old `#define` macros to predefine colors, so that changing the website's
color palette is easier.
