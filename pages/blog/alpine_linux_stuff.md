% assorted alpine linux stuff
% /dev/urandom
% june 2020

# About Alpine Linux

[Alpine Linux](https://alpinelinux.org) is a minimalist Linux distro built with
musl libc (as opposed to glibc), OpenRC as an init system and busybox as the
default set of shell utilities. It is frequently used in Docker containers due
to its small size, but it can also make a good workstation distribution, if you
are willing to make some sacrifices. (In fact, I use it to run my personal work
computers.)

Its package manager, `apk`, is fast and easy to use, and creating one's own
packages is performed by building from an `APKBUILD` file very similar to Arch
Linux's `PKGBUILD`s.

This page will include various information that I found useful regarding this
OS.

## vim shell

By default, the shell is `/bin/ash`, which is a hardlink to busybox. Vim's
default shellpipe options don't account for that, which results in `:make` and
other similar commands missing any text outputted to stderr. Use

    set shell=/bin/sh

in your .vimrc to set the shell to `/bin/sh` and fix that.
