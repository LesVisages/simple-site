% riichi mahjong cheatsheet
% /dev/urandom
% may 2020

<pre>
   ╔═══════════════════════════╤═══╤═══╤╤═══════════════════════════╤═══╤═══╗
   ║ yaku                      │cls│opn││ honitsu (1 suit & honors) │ 3 │ 2 ║ 
   ╟───────────────────────────┼───┼───┤│ junchan (19 in each)      │ 3 │ 2 ║
   ║ menzen tsumo              │ 1 │ ─ ││ ryanpeikou (2x iipeikou)  │ 3 │ ─ ║
   ║ riichi                    │ 1 │ - │├───────────────────────────┼───┼───╢
   ║ riichi ippatsu            │ 1 │ - ││ chinitsu (full flush)     │ 6 │ 5 ║
   ║ pinfu                     │ 1 │ ─ │├───────────────────────────┼───┼───╢
   ║ iipeikou (2 ident. seq.)  │ 1 │ ─ ││ kazoe (>= 13 han)         │ Y │ Y ║
   ╟───────────────────────────┼───┼───┤│ kokushi (13 orphans)      │ Y │ - ║
   ║ haitei raoyue (last wall) │ 1 │ 1 ││ suuankou (4 closed trips) │ Y │ - ║
   ║ houtei raoyui (last dis.) │ 1 │ 1 ││ dai3gen (3 dragon trips)  │ Y │ Y ║
   ║ rinshan (win from dead w) │ 1 │ 1 ││ shousuushi (3x3+2 winds)  │ Y │ Y ║
   ║ chankan (stealing a kan)  │ 1 │ 1 ││ daisuushi (4x3 winds)     │ Y │ Y ║
   ║ tanyao (all simples)      │ 1 │ 1 ││ tsuuiisou (all DW)        │ Y │ Y ║
   ║ yakuhai (D or seat/rnd W) │ 1 │ 1 ││ chinroutou (all 19)       │ Y │ Y ║
   ╟───────────────────────────┼───┼───┤│ ryuuiisou (all greens)    │ Y │ Y ║
   ║ 2x riichi (from start)    │ 2 │ - ││ chuuren poutou (flush     │   │   ║
   ║ chantaiyao (19DW in each) │ 2 │ 1 ││      1112345678999+pair)  │ Y │ Y ║
   ║ 3shoku doujun (3col seq)  │ 2 │ 1 ││ suukantsu (4 kans)        │ Y │ Y ║
   ║ ittsu (1~9 straight)      │ 2 │ 1 │├───────────────────────────┼───┼───╢
   ║ toitoi (all triplets)     │ 2 │ 2 ││ tenhou (win at start)     │ Y │ - ║
   ║ 3ankou (3 closed trips)   │ 2 │ 2 ││ chiihou (win at 1st draw) │ Y │ - ║
   ║ 3shoku doukou (3col trip) │ 2 │ 2 │├───────────────────────────┼───┼───╢
   ║ 3kantsu (3 kans)          │ 2 │ 2 ││ nagashi (all 19DW disc.)  │ M │ - ║
   ║ chiitoitsu (7 pairs)      │ 2 │ X │├───────────────────────────┼───┼───╢
   ║ honroutou (all 19DW)      │ 2 │ 2 ││*renhou (ron before        │   │   ║
   ║ shou3gen (DDD, DDD, DD)   │ 2 │ 2 ││        dealer's 2nd turn) │ M │ - ║  
   ╚═══════════════════════════╧═══╧═══╧╧═══════════════════════════╧═══╧═══╝

╒══════════════════════════════════════════════════════════════════════════════╕
│ BASIC SCORE: score = fu * 2^(2+han)                                          │
└──────────────────────────────────────────────────────────────────────────────┘
 ┌─────┬──────┬──────┬──────┬──────┬──────┬──────┬──────┬──────┬──────┬──────┐
 │ han │   20 │   25 │   30 │   40 │   50 │   60 │   70 │   80 │   90 │  +10 │
 ├─────┼──────┼──────┼──────┼──────┼──────┼──────┼──────┼──────┼──────┼──────┤
 │  1  │      │      │  240 │  320 │  400 │  480 │  560 │  640 │  720 │  +80 │
 │  2  │  320 │      │  480 │  640 │  800 │  960 │ 1120 │ 1280 │ 1440 │ +160 │
 │  3  │  640 │  800 │  960 │ 1280 │ 1600 │ 1920 │─────────> 2000 <──────────│
 │  4  │ 1280 │ 1600 │ 1920 │────────────────────> 2000 <────────────────────│
 ├─────┼──────┴──────┴──────┼────────────────────────────────────────────────┤
 │  5  │ mangan (2000)      │ RON: loser pays winner BSx4 ↑ 100.             │
 │ 6,7 │ haneman (3000)     │      if dealer won, loser pays BSx6 ↑ 100.     │
 │ 8~10│ baiman (4000)      │ TSUMO: everybody pays BS ↑ 100,                │
 │11,12│ sanbaiman (6000)   │         except dealer pays BSx2 ↑ 100.         │
 │ 13+ │ yakuman (8000)     │       if dealer won, everybody pays BSx2 ↑ 100.│
 └─────┴────────────────────┴────────────────────────────────────────────────┘

╒══════════════════════════════════════════════════════════════════════════════╕
│ FU CALCULATION: fu = basic + sets + value + waits + tsumo + open no fu       │
└──────────────────────────────────────────────────────────────────────────────┘
 basic points: 30 for closed ron, 25 for chiitoitsu, 20 otherwise

 pon: +2 fu. x2 for closed, x2 for terminal or honor
 kan: +8 fu. x2 for closed, x2 for terminal or honor

 value tiles (dragon and seat/round wind): +2 fu,
	+4 fu if tile == seat wind == round wind

 kanchan (middle wait), penchan (edge wait) or tanki (pair wait): +2 fu

 tsumo (except pinfu): +2 fu.
 no additional fu in an open hand: +2 fu.

 resulting number of fu is rounded up to 10.
</pre>
