% assorted shell scripts
% /dev/urandom
% may 2020

shell scripts:

grab lines from dictionary, convert them into up-to-16-byte strings, do a binary dump

    for x in `cat input_words.txt`; do echo $x | tr -d \\n | dd ibs=16 conv=sync 2>/dev/null | xxd; done

convert a .vgz directory to a .vgm one, along with updating all the playlists:

    rename ".vgz" ".vgm.gz" *.vgz; gzip -d *.vgm.gz; sed -i "s/\.vgz/\.vgm/" *.m3u
		
(requires rename from `util-linux` and not from perl)
