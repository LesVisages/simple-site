% описание и примеры письменности sitelen pona 
% /dev/urandom
% june 2020

<style>
@font-face {
    font-family: "sitelen pona";
    src: url("linjapimeja19.woff")
}

.sp {
    font-size:3em;
    font-family:"sitelen pona";
    font-variant-ligatures: common-ligatures;
}

.spflex {
display: none;
flex-direction: row;
flex-wrap: wrap;
border: 2px solid #9b9b9b;
border-radius: 4pt;
}

.spitem {
	flex-grow: 1;
	width: 4em;
	padding: 0.25em;
	margin: 0.5em;
	border: 1px solid #9b9b9b;
	border-radius: 4pt;
	text-align: center;
	overflow: hidden;
}

@media tty {
	.spflex {
		display: none !important;
	}
}

</style>

## sitelen pona

"sitelen pona" ("простое письмо" или "хорошее письмо") -- логографическая
письменность для языка токи пона, разработанная автором языка, Соней Лэнг.

> %info%
> Раздел официальной страницы, описывающий sitelen pona, опубликован под
> некоммерческой лицензией [CC-BY-NC
> 4.0](https://creativecommons.org/licenses/by-nc/4.0/). Как результат, этот
> раздел легко найти в других сетевых курсах. Страница o sitelen pona в курсе
> ["o kama sona e toki pona!"](http://tokipona.net/tp/janpije/hieroglyphs.php)
> почти полностью повторяет содержимое официальной книги.

### логографическое письмо

Логография, или логографическое письмо -- это система, в которой **каждый символ
представляет собой отдельное слово** (или иногда даже фразу). Самая известная
логография в мире -- это ханьцзы -- письменность, используемая в китайском и (в
дополнении к их собственным письменностям) также японском и корейском языках.

> %info%
> Логографическое письмо отлично подходит для языков, в которых слова не
> склоняются и не спрягаются, а предложения строятся путём составления слов в
> правильном порядке (такие языки называются "изолирующими"). И китайский язык,
> и токи пона являются изолирующими языками.

Но в отличие от китайского языка, для базового владения которым нужно знать как
минимум 1500 слов и соответствующих им символов, **120 слов** токи пона выучить
**намного проще** -- особенно учитывая то, что иероглифы sitelen pona почти все
визуально обозначают их слова -- слово "lawa" ("голова") изображается, как
голова с кепкой. Слово "nanpa" ("число, номер") выглядит, как (англоязычный)
символ номера (\#), и так далее. (Хотя стоит заметить, что некоторые из символов
ханьцзы также являются иероглифами, изображающими их значения.)

### таблица sitelen pona

<noscript>
<div class="error">
В вашем веб-браузере выключен JavaScript. Без скриптов невозможно определить,
загрузился ли шрифт linja pimeja или нет. Из-за этого таблица sitelen pona будет
скрыта.
</div>
</noscript>

<div class="spflex">
<div class="spitem"><div class="sp">a</div>		a/kin</div>
<div class="spitem"><div class="sp">akesi</div>		akesi</div>
<div class="spitem"><div class="sp">ala</div>		ala</div>
<div class="spitem"><div class="sp">alasa</div>		alasa</div>
<div class="spitem"><div class="sp">ale</div>		ale/ali</div>
<div class="spitem"><div class="sp">anpa</div>		anpa</div>
<div class="spitem"><div class="sp">ante</div>		ante</div>
<div class="spitem"><div class="sp">anu</div>		anu</div>
<div class="spitem"><div class="sp">awen</div>		awen</div>
<div class="spitem"><div class="sp">e</div>		e</div>
<div class="spitem"><div class="sp">en</div>		en</div>
<div class="spitem"><div class="sp">esun</div>		esun</div>
<div class="spitem"><div class="sp">ijo</div>		ijo</div>
<div class="spitem"><div class="sp">ike</div>		ike</div>
<div class="spitem"><div class="sp">ilo</div>		ilo</div>
<div class="spitem"><div class="sp">insa</div>		insa</div>
<div class="spitem"><div class="sp">jaki</div>		jaki</div>
<div class="spitem"><div class="sp">jan</div>		jan</div>
<div class="spitem"><div class="sp">jelo</div>		jelo</div>
<div class="spitem"><div class="sp">jo</div>		jo</div>
<div class="spitem"><div class="sp">kala</div>		kala</div>
<div class="spitem"><div class="sp">kalama</div>	kalama</div>
<div class="spitem"><div class="sp">kama</div>		kama</div>
<div class="spitem"><div class="sp">kasi</div>		kasi</div>
<div class="spitem"><div class="sp">ken</div>		ken</div>
<div class="spitem"><div class="sp">kepeken</div>	kepeken</div>
<div class="spitem"><div class="sp">kili</div>		kili</div>
<div class="spitem"><div class="sp">kiwen</div>		kiwen</div>
<div class="spitem"><div class="sp">ko</div>		ko</div>
<div class="spitem"><div class="sp">kon</div>		kon</div>
<div class="spitem"><div class="sp">kule</div>		kule</div>
<div class="spitem"><div class="sp">kulupu</div>	kulupu</div>
<div class="spitem"><div class="sp">kute</div>		kute</div>
<div class="spitem"><div class="sp">la</div>		la</div>
<div class="spitem"><div class="sp">lape</div>		lape</div>
<div class="spitem"><div class="sp">laso</div>		laso</div>
<div class="spitem"><div class="sp">lawa</div>		lawa</div>
<div class="spitem"><div class="sp">len</div>		len</div>
<div class="spitem"><div class="sp">lete</div>		lete</div>
<div class="spitem"><div class="sp">li</div>		li</div>
<div class="spitem"><div class="sp">lili</div>		lili</div>
<div class="spitem"><div class="sp">linja</div>		linja</div>
<div class="spitem"><div class="sp">lipu</div>		lipu</div>
<div class="spitem"><div class="sp">loje</div>		loje</div>
<div class="spitem"><div class="sp">lon</div>		lon</div>
<div class="spitem"><div class="sp">luka</div>		luka</div>
<div class="spitem"><div class="sp">lukin</div>		lukin</div>
<div class="spitem"><div class="sp">lupa</div>		lupa</div>
<div class="spitem"><div class="sp">ma</div>		ma</div>
<div class="spitem"><div class="sp">mama</div>		mama</div>
<div class="spitem"><div class="sp">mani</div>		mani</div>
<div class="spitem"><div class="sp">meli</div>		meli</div>
<div class="spitem"><div class="sp">mi</div>		mi</div>
<div class="spitem"><div class="sp">mije</div>		mije</div>
<div class="spitem"><div class="sp">moku</div>		moku</div>
<div class="spitem"><div class="sp">moli</div>		moli</div>
<div class="spitem"><div class="sp">monsi</div>		monsi</div>
<div class="spitem"><div class="sp">mu</div>		mu</div>
<div class="spitem"><div class="sp">mun</div>		mun</div>
<div class="spitem"><div class="sp">musi</div>		musi</div>
<div class="spitem"><div class="sp">mute</div>		mute</div>
<div class="spitem"><div class="sp">nanpa</div>		nanpa</div>
<div class="spitem"><div class="sp">nasa</div>		nasa</div>
<div class="spitem"><div class="sp">nasin</div>		nasin</div>
<div class="spitem"><div class="sp">nena</div>		nena</div>
<div class="spitem"><div class="sp">ni</div>		ni</div>
<div class="spitem"><div class="sp">nimi</div>		nimi</div>
<div class="spitem"><div class="sp">noka</div>		noka</div>
<div class="spitem"><div class="sp">o</div>		o</div>
<div class="spitem"><div class="sp">olin</div>		olin</div>
<div class="spitem"><div class="sp">ona</div>		ona</div>
<div class="spitem"><div class="sp">open</div>		open</div>
<div class="spitem"><div class="sp">pakala</div>	pakala</div>
<div class="spitem"><div class="sp">pali</div>		pali</div>
<div class="spitem"><div class="sp">palisa</div>	palisa</div>
<div class="spitem"><div class="sp">pan</div>		pan</div>
<div class="spitem"><div class="sp">pana</div>		pana</div>
<div class="spitem"><div class="sp">pi</div>		pi</div>
<div class="spitem"><div class="sp">pilin</div>		pilin</div>
<div class="spitem"><div class="sp">pimeja</div>	pimeja</div>
<div class="spitem"><div class="sp">pini</div>		pini</div>
<div class="spitem"><div class="sp">pipi</div>		pipi</div>
<div class="spitem"><div class="sp">poka</div>		poka</div>
<div class="spitem"><div class="sp">poki</div>		poki</div>
<div class="spitem"><div class="sp">pona</div>		pona</div>
<div class="spitem"><div class="sp">pu</div>		pu</div>
<div class="spitem"><div class="sp">sama</div>		sama</div>
<div class="spitem"><div class="sp">seli</div>		seli</div>
<div class="spitem"><div class="sp">selo</div>		selo</div>
<div class="spitem"><div class="sp">seme</div>		seme</div>
<div class="spitem"><div class="sp">sewi</div>		sewi</div>
<div class="spitem"><div class="sp">sijelo</div>	sijelo</div>
<div class="spitem"><div class="sp">sike</div>		sike</div>
<div class="spitem"><div class="sp">sin</div>		sin</div>
<div class="spitem"><div class="sp">sina</div>		sina</div>
<div class="spitem"><div class="sp">sinpin</div>	sinpin</div>
<div class="spitem"><div class="sp">sitelen</div>	sitelen</div>
<div class="spitem"><div class="sp">sona</div>		sona</div>
<div class="spitem"><div class="sp">soweli</div>	soweli</div>
<div class="spitem"><div class="sp">suli</div>		suli</div>
<div class="spitem"><div class="sp">suno</div>		suno</div>
<div class="spitem"><div class="sp">supa</div>		supa</div>
<div class="spitem"><div class="sp">suwi</div>		suwi</div>
<div class="spitem"><div class="sp">tan</div>		tan</div>
<div class="spitem"><div class="sp">taso</div>		taso</div>
<div class="spitem"><div class="sp">tawa</div>		tawa</div>
<div class="spitem"><div class="sp">telo</div>		telo</div>
<div class="spitem"><div class="sp">tenpo</div>		tenpo</div>
<div class="spitem"><div class="sp">toki</div>		toki</div>
<div class="spitem"><div class="sp">tomo</div>		tomo</div>
<div class="spitem"><div class="sp">tu</div>		tu</div>
<div class="spitem"><div class="sp">unpa</div>		unpa</div>
<div class="spitem"><div class="sp">uta</div>		uta</div>
<div class="spitem"><div class="sp">utala</div>		utala</div>
<div class="spitem"><div class="sp">walo</div>		walo</div>
<div class="spitem"><div class="sp">wan</div>		wan</div>
<div class="spitem"><div class="sp">waso</div>		waso</div>
<div class="spitem"><div class="sp">wawa</div>		wawa</div>
<div class="spitem"><div class="sp">weka</div>		weka</div>
<div class="spitem"><div class="sp">wile</div>		wile</div>
</div>

<script>
document.fonts.load("12pt 'sitelen pona'").then(function () {
  var box = document.getElementsByClassName('spflex');
  for (var i=0; i < box.length; i++)
   box[i].style.display = "flex"; 
});
</script>

Как и латиница с кириллицей (или даже китайская письменность), sitelen pona
пишется слева направо и сверху вниз. Каждое слово пишется соответствующим ему
символом без дополнительных пробелов.

Символ прилагательного допускается писать внутри (или сверху/снизу) символа
соответствующего ему существительного.

> %info%
> "Логотип" языка токи пона, используемый в официальной книге и на большинстве
> вебсайтов, включая этот, является такого рода совместным иероглифом для фразы
> "toki pona": символ "pona" нарисован внутри символа "toki".

Неофициальные слова (имена собственные и названия) пишутся внутри картуши
(длинного овального символа), где каждый иероглиф соответствует лишь начальной
букве его слова. Например, в официальной книге и на странице курса "o kama sona
e toki pona!" фраза "ma Kanata" (страна Канада) написано как "ma [kasi alasa 
nasin awen telo a]". (В некоторых случаях, вместо картуши могут использоваться
скобки).

### применение sitelen pona на практике

> %info%
> Информация в этом разделе не является частью официального дизайна письменности
> sitelen pona, а основана на том, как эта письменность используется в
> сообществе токи пона.

Предложения разделяются либо точкой, либо пробелом. Все другие знаки препинания
(запятые, двоеточия и т.д.) могут быть написаны или не написаны (в большинстве
случаев их наличие или отсутствие не меняет смысл предложений).

Слова, созданные сообществом токи пона и не входящие в официальный словарь,
обычно пишутся не как "неофициальные слова" (обычно применяемые для имён
собственных), а используют свои собственные символы.

Поскольку вопросительный знак используется как иероглиф для слова "seme",
вопросительные предложения могут заканчиваться точкой или вопросительным знаком
меньшего размера.

### Примеры

> %warning%
> (Если ваш веб-браузер не смог загрузить [шрифт "linja pimeja"](https://github.com/increpare/linja_pimeja), 
> то текст внизу будет написан латиницей.

> %sp%
> wan ni pi lipu ni li sitelen kepeken sitelen pona. sina ken ala ken sona e ni.
>

<a name="answers" href="#answers" onclick="revealSpoilers();">Раскрыть перевод</a>

> %spoiler%
> Эта часть этого документа написана с помощью sitelen pona. Можете ли вы её
> понять?

Хорошим ресурсом текстов, написанных с sitelen pona, включая страницу,
пытающуюся обучить кого-то письменности без использования какой-либо другой
письменности, является сайт ["tomo pi sitelen
pona"](https://davidar.github.io/tp/), созданный jan Tepu.

### Шрифты

Текст sitelen pona на этой странице отображается шрифтом под названием "linja
pimeja". Однако это не единственный шрифт, который используется для этой
письменности в сети. Есть и другие варианты, например:

 * Шрифт "[linja pona](musilili.net/linja-pona/)" имеет очень простой дизайн и
   поддерживает огромное количество составных иероглифов. Он является наиболее
   популярным из всех.

 * Шрифт "[sitelen pona pona](https://jackhumbert.github.io/sitelen-pona-pona/)"
   имеет множество символов, выглядящих отлично от обычной sitelen pona, но зато
   легко читается в разных размерах и не требует каким-либо образом
   модифицировать текст для правильного его отображения. Лично я считаю его моим
   любимым шрифтом для sitelen pona.

> %info%
> В частности, на тех веб-страницах этого сайта, где доступен выбор между
> латиницей и sitelen pona, используется именно шрифт "sitelen pona pona", так
> как он отлично работает с немодифицированным текстом и словами, отсутствующими
> в словаре.

 * Мой собственный шрифт sitelen pona называется "[insa pi supa lape](supalape.html)". 
   Он основан на шрифте "Bedstead" и представляет собой растровый шрифт,
   алгоритмически преобразованный в векторный формат.

### sitelen emoji / sitelen pilin

Письменность "sitelen emoji" (также известная как "sitelen pilin") является
адаптацией sitelen pona, использующей уже существующие символы эмодзи (emoji)
для каждого из символов в sitelen pona. Благодаря этому эту адаптацию можно с
лёгкостью использовать в большинстве веб-браузеров и приложений.

* [Официальная страница](https://sites.google.com/view/sitelenemoji)

* [Описание системы, включая версию таблицы для Windows и примеры текстов] (
https://omniglot.com/conscripts/sitelenemoji.htm)

---

[Страница о других письменностях](ru_x2.html)

[Главная страница](ru_index.html)


