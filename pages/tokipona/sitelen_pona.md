% sitelen pona description and examples
% /dev/urandom
% june 2020

<style>
@font-face {
    font-family: "sitelen pona";
    src: url("linjapimeja19.woff")
}

.sp {
    font-size:3em;
    font-family:"sitelen pona";
    font-variant-ligatures: common-ligatures;
}

.spflex {
display: none;
flex-direction: row;
flex-wrap: wrap;
border: 2px solid #9b9b9b;
border-radius: 4pt;
}

.spitem {
	flex-grow: 1;
	width: 4em;
	padding: 0.25em;
	margin: 0.5em;
	border: 1px solid #9b9b9b;
	border-radius: 4pt;
	text-align: center;
	overflow: hidden;
}

@media tty {
	.spflex {
		display: none !important;
	}
}

</style>

## sitelen pona

"sitelen pona" ("simple writing" or "good writing") is a logographic writing
system designed for toki pona by its creator, Sonja Lang. 

> %info%
> The part of the official book describing sitelen pona was published with a
> non-commercial [CC-BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/) 
> license. Hence, it's easily available online in other courses,
> such as ["o kama sona e toki pona!"'s
> page](http://tokipona.net/tp/janpije/hieroglyphs.php) on the system, which
> describes it almost exactly the same as the official book.
>

### logographic systems

In a logographic system, **each character generally represents one word** (or
sometimes even a phrase). The most commonly known example of a logographic
system are the Han characters, used in Chinese and (in addition to their own
writing systems) Japanese and Korean.

> %info%
> Logographic systems are well-suited for languages in which words have little
> to no inflection (change very little, if not at all, based on grammar), and
> the grammar is instead based around putting existing words together (such a
> language is called "isolating"). Chinese languages fit that idea really well,
> as does toki pona.
>

But since toki pona's basic dictionary only uses **120 words** (plus a few
community additions), a logographic system for toki pona also becomes
**significantly easier to learn** and use than that of Chinese, which requires
knowing at least 1500 characters to achieve fluency. In addition, most
characters in sitelen pona visually represent the words they mean -- for
example, "lawa", meaning "head", is literally a symbol of a head with a cap on.
"nanpa", meaning "number", is based on the "\#" number sign, and so on. (To some
extent, the same is true for a number of Han characters as well.)

### sitelen pona chart

<noscript>
<div class="error">
Your web browser has JavaScript disabled. Without it, this page cannot check
whether or not the "linja pimeja" font has successfully loaded. Therefore, the
sitelen pona table will remain hidden.
</div>
</noscript>

<div class="spflex">
<div class="spitem"><div class="sp">a</div>		a/kin</div>
<div class="spitem"><div class="sp">akesi</div>		akesi</div>
<div class="spitem"><div class="sp">ala</div>		ala</div>
<div class="spitem"><div class="sp">alasa</div>		alasa</div>
<div class="spitem"><div class="sp">ale</div>		ale/ali</div>
<div class="spitem"><div class="sp">anpa</div>		anpa</div>
<div class="spitem"><div class="sp">ante</div>		ante</div>
<div class="spitem"><div class="sp">anu</div>		anu</div>
<div class="spitem"><div class="sp">awen</div>		awen</div>
<div class="spitem"><div class="sp">e</div>		e</div>
<div class="spitem"><div class="sp">en</div>		en</div>
<div class="spitem"><div class="sp">esun</div>		esun</div>
<div class="spitem"><div class="sp">ijo</div>		ijo</div>
<div class="spitem"><div class="sp">ike</div>		ike</div>
<div class="spitem"><div class="sp">ilo</div>		ilo</div>
<div class="spitem"><div class="sp">insa</div>		insa</div>
<div class="spitem"><div class="sp">jaki</div>		jaki</div>
<div class="spitem"><div class="sp">jan</div>		jan</div>
<div class="spitem"><div class="sp">jelo</div>		jelo</div>
<div class="spitem"><div class="sp">jo</div>		jo</div>
<div class="spitem"><div class="sp">kala</div>		kala</div>
<div class="spitem"><div class="sp">kalama</div>	kalama</div>
<div class="spitem"><div class="sp">kama</div>		kama</div>
<div class="spitem"><div class="sp">kasi</div>		kasi</div>
<div class="spitem"><div class="sp">ken</div>		ken</div>
<div class="spitem"><div class="sp">kepeken</div>	kepeken</div>
<div class="spitem"><div class="sp">kili</div>		kili</div>
<div class="spitem"><div class="sp">kiwen</div>		kiwen</div>
<div class="spitem"><div class="sp">ko</div>		ko</div>
<div class="spitem"><div class="sp">kon</div>		kon</div>
<div class="spitem"><div class="sp">kule</div>		kule</div>
<div class="spitem"><div class="sp">kulupu</div>	kulupu</div>
<div class="spitem"><div class="sp">kute</div>		kute</div>
<div class="spitem"><div class="sp">la</div>		la</div>
<div class="spitem"><div class="sp">lape</div>		lape</div>
<div class="spitem"><div class="sp">laso</div>		laso</div>
<div class="spitem"><div class="sp">lawa</div>		lawa</div>
<div class="spitem"><div class="sp">len</div>		len</div>
<div class="spitem"><div class="sp">lete</div>		lete</div>
<div class="spitem"><div class="sp">li</div>		li</div>
<div class="spitem"><div class="sp">lili</div>		lili</div>
<div class="spitem"><div class="sp">linja</div>		linja</div>
<div class="spitem"><div class="sp">lipu</div>		lipu</div>
<div class="spitem"><div class="sp">loje</div>		loje</div>
<div class="spitem"><div class="sp">lon</div>		lon</div>
<div class="spitem"><div class="sp">luka</div>		luka</div>
<div class="spitem"><div class="sp">lukin</div>		lukin</div>
<div class="spitem"><div class="sp">lupa</div>		lupa</div>
<div class="spitem"><div class="sp">ma</div>		ma</div>
<div class="spitem"><div class="sp">mama</div>		mama</div>
<div class="spitem"><div class="sp">mani</div>		mani</div>
<div class="spitem"><div class="sp">meli</div>		meli</div>
<div class="spitem"><div class="sp">mi</div>		mi</div>
<div class="spitem"><div class="sp">mije</div>		mije</div>
<div class="spitem"><div class="sp">moku</div>		moku</div>
<div class="spitem"><div class="sp">moli</div>		moli</div>
<div class="spitem"><div class="sp">monsi</div>		monsi</div>
<div class="spitem"><div class="sp">mu</div>		mu</div>
<div class="spitem"><div class="sp">mun</div>		mun</div>
<div class="spitem"><div class="sp">musi</div>		musi</div>
<div class="spitem"><div class="sp">mute</div>		mute</div>
<div class="spitem"><div class="sp">nanpa</div>		nanpa</div>
<div class="spitem"><div class="sp">nasa</div>		nasa</div>
<div class="spitem"><div class="sp">nasin</div>		nasin</div>
<div class="spitem"><div class="sp">nena</div>		nena</div>
<div class="spitem"><div class="sp">ni</div>		ni</div>
<div class="spitem"><div class="sp">nimi</div>		nimi</div>
<div class="spitem"><div class="sp">noka</div>		noka</div>
<div class="spitem"><div class="sp">o</div>		o</div>
<div class="spitem"><div class="sp">olin</div>		olin</div>
<div class="spitem"><div class="sp">ona</div>		ona</div>
<div class="spitem"><div class="sp">open</div>		open</div>
<div class="spitem"><div class="sp">pakala</div>	pakala</div>
<div class="spitem"><div class="sp">pali</div>		pali</div>
<div class="spitem"><div class="sp">palisa</div>	palisa</div>
<div class="spitem"><div class="sp">pan</div>		pan</div>
<div class="spitem"><div class="sp">pana</div>		pana</div>
<div class="spitem"><div class="sp">pi</div>		pi</div>
<div class="spitem"><div class="sp">pilin</div>		pilin</div>
<div class="spitem"><div class="sp">pimeja</div>	pimeja</div>
<div class="spitem"><div class="sp">pini</div>		pini</div>
<div class="spitem"><div class="sp">pipi</div>		pipi</div>
<div class="spitem"><div class="sp">poka</div>		poka</div>
<div class="spitem"><div class="sp">poki</div>		poki</div>
<div class="spitem"><div class="sp">pona</div>		pona</div>
<div class="spitem"><div class="sp">pu</div>		pu</div>
<div class="spitem"><div class="sp">sama</div>		sama</div>
<div class="spitem"><div class="sp">seli</div>		seli</div>
<div class="spitem"><div class="sp">selo</div>		selo</div>
<div class="spitem"><div class="sp">seme</div>		seme</div>
<div class="spitem"><div class="sp">sewi</div>		sewi</div>
<div class="spitem"><div class="sp">sijelo</div>	sijelo</div>
<div class="spitem"><div class="sp">sike</div>		sike</div>
<div class="spitem"><div class="sp">sin</div>		sin</div>
<div class="spitem"><div class="sp">sina</div>		sina</div>
<div class="spitem"><div class="sp">sinpin</div>	sinpin</div>
<div class="spitem"><div class="sp">sitelen</div>	sitelen</div>
<div class="spitem"><div class="sp">sona</div>		sona</div>
<div class="spitem"><div class="sp">soweli</div>	soweli</div>
<div class="spitem"><div class="sp">suli</div>		suli</div>
<div class="spitem"><div class="sp">suno</div>		suno</div>
<div class="spitem"><div class="sp">supa</div>		supa</div>
<div class="spitem"><div class="sp">suwi</div>		suwi</div>
<div class="spitem"><div class="sp">tan</div>		tan</div>
<div class="spitem"><div class="sp">taso</div>		taso</div>
<div class="spitem"><div class="sp">tawa</div>		tawa</div>
<div class="spitem"><div class="sp">telo</div>		telo</div>
<div class="spitem"><div class="sp">tenpo</div>		tenpo</div>
<div class="spitem"><div class="sp">toki</div>		toki</div>
<div class="spitem"><div class="sp">tomo</div>		tomo</div>
<div class="spitem"><div class="sp">tu</div>		tu</div>
<div class="spitem"><div class="sp">unpa</div>		unpa</div>
<div class="spitem"><div class="sp">uta</div>		uta</div>
<div class="spitem"><div class="sp">utala</div>		utala</div>
<div class="spitem"><div class="sp">walo</div>		walo</div>
<div class="spitem"><div class="sp">wan</div>		wan</div>
<div class="spitem"><div class="sp">waso</div>		waso</div>
<div class="spitem"><div class="sp">wawa</div>		wawa</div>
<div class="spitem"><div class="sp">weka</div>		weka</div>
<div class="spitem"><div class="sp">wile</div>		wile</div>
</div>

<script>
document.fonts.load("12pt 'sitelen pona'").then(function () {
  var box = document.getElementsByClassName('spflex');
  for (var i=0; i < box.length; i++)
   box[i].style.display = "flex"; 
});
</script>

Much like the Latin alphabet, it is written left-to-right and top-to-bottom.
Each toki pona word is written using its character, without extra spaces between
words.

An adjective character can be put inside or over/under a noun character to
represent a noun phrase.

> %info%
> You might notice that toki pona's "logo", used on the cover of the official
> book and on most websites to represent it (including this one), is, in fact, sitelen pona's
> composite character for "toki pona", with the "pona" symbol written inside the
> "toki" symbol.

Unofficial words are written inside a cartouche (a rounded shape that
surrounds all the characters), with characters for words that start with their
first letters. For an example, in the page linked at the beginning (and used in
the official book), "ma Kanata" is written as "ma [kasi alasa nasin awen telo
a]". (In some fonts, the cartouche may be replaced with parentheses or brackets
between the characters.)

### sitelen pona as commonly used

> %info%
> The information in this part is not part of the official design of sitelen
> pona. It is based entirely on how sitelen pona is used by the toki pona
> community.

Sentences are separated either with a dot or with a space. All other punctuation
(commas, colons, etc.) is either omitted or written as their corresponding
characters (since in practically all cases, their presence or absence doesn't
change the meaning of a sentence).

Words added by the toki pona community usually have their own separate
characters and are not written as unofficial words.

Since the question mark is used as the character for "seme", question sentences
may be ended with a period (or a smaller question mark) instead.

### Examples

Here's some basic text written in sitelen pona. 

> %warning%
> (If your browser is unable to load the ["linja pimeja" font](https://github.com/increpare/linja_pimeja), 
> the text below would just show up in large Latin characters.

> %sp%
> wan ni pi lipu ni li sitelen kepeken sitelen pona. sina ken ala ken sona e ni.
>

<a name="answers" href="#answers" onclick="revealSpoilers();">Reveal translation</a>

> %spoiler%
> This part of this document is written using sitelen pona. Can you understand
> it?

For some other texts written in sitelen pona, including a page that tries to
teach someone to read it without using any other writing system, check out the
website ["tomo pi sitelen pona"](https://davidar.github.io/tp/) by jan Tepu.

### Fonts

The text above is displayed using a font called "linja pimeja". However, for
displaying sitelen pona text online, there is a ton of other options. Here are
the most common ones.

 * A font called "[linja pona](musilili.net/linja-pona/)" is characteristic for
   its basic design and support for tons and tons of different composite
   characters. It is the most popular option.

 * "[sitelen pona pona](https://jackhumbert.github.io/sitelen-pona-pona/)" is a
   font that features some characters way different from regular sitelen pona,
   but looks very nice on different font sizes and doesn't require any
   modification to toki pona text in order to look good. In particular, this is
   my favorite font.

> %info%
> Some pages on this website may offer an ability to toggle between Latin and
> sitelen pona displays. The latter option will use the "sitelen pona pona"
> font, since it works best with unmodified toki pona texts and falls back
> nicely in texts that use non-pu, unofficial or outright non-toki-pona
> words in them.
>

 * I have also designed a font for sitelen pona, called "[insa pi supa
 lape](supalape.html)". It is based on the font "Bedstead"(hence the name) and
uses the same algorithm to convert small bitmaps of different characters into a
fully-functional vector font.

### sitelen emoji / sitelen pilin

A system called "sitelen emoji" (or "sitelen pilin") adapts sitelen pona by
using an emoji character for each of the possible sitelen pona characters. This
makes it possible to use it in most web browsers and messenger apps.

* [Official page](https://sites.google.com/view/sitelenemoji)

* [Description of the system, as well as a Windows emoji character chart and
example text](https://omniglot.com/conscripts/sitelenemoji.htm)

---

[Page about other alternative writing systems](x2.html)

[Top page](index.html)


