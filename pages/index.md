% rnd's website
% /dev/urandom
% date unknown

<style>
.header h3, .titlesep {
	display: none;
}
</style>

# Hello and welcome!

Welcome to my little website! I'm running it mostly as an experiment in static
website generation, but also to host some content that others might find
interesting.

* [lipu sona pona - a toki pona course](tokipona/)

* [blog thingy](blog/)

* [rnd's games](games.html)

* [building a static website](static.html)

* [random links](links.html)

> %warning%
> This is yet another of those "under construction" messages you'd see on
> pre-Web 2.0 websites.

