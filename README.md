# rnd's website source code

This is the source code to rnd's website at <https://rnd.neocities.org/>.

Included are all the files and scripts used to build it, with the exception of
the `upload.pwd` file containing the account password and possibly some files
containing other identifying information.
