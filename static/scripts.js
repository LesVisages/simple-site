function revealSpoilers() {
	var s = document.getElementsByClassName('spoiler');

	for (var i=0; i < s.length; i++)
	  s[i].style.visibility = 'visible';
}

function toggle_sitelen_pona(id) {

	let elements_1 = document.querySelectorAll(".pona0");
	let elements_2 = document.querySelectorAll(".pona");
	
	for (let i=0; i < elements_1.length; i++) {
		elements_1[i].classList.add("pona");
		elements_1[i].classList.remove("pona0");
	}

	for (let i=0; i < elements_2.length; i++) {
		elements_2[i].classList.add("pona0");
		elements_2[i].classList.remove("pona");
	}

}

function internet_time(dt)
{
  return (((dt.getUTCHours() + 1) % 24) + dt.getUTCMinutes() / 60 + dt.getUTCSeconds() / 3600) * 1000 / 24;
}

function update_clock()
{
	var dt = new Date();
	var it = internet_time(dt);
	document.getElementById("itime-value").innerHTML = Math.floor(it);
	document.getElementById("itime-progress").style.width = (it - Math.floor(it)) * 100 + "%";
}

function update_every_second()
{
	update_clock();
	var t = setTimeout(function() {update_every_second() }, 1000); //timer
}

update_every_second();
